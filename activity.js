db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

//Total number of fruits on sale

db.fruits.aggregate([ 
	{$match: { onSale: true}},
	{$count: "fruitsOnSale"}
]);

//Total number of fruits with greater than 20 value

db.fruits.aggregate([ 
	{$match: { stock: {$gte: 20}}},
	{$count: "enoughStock"}
]);

//Average price of fruits onSale per supplier
db.fruits.aggregate([ 
	{$match: { onSale: true}},
	{$group : { _id: "$supplier_id", avg_price: {$avg: "$price" }}}
]);


//Max price of fruit per supplier

db.fruits.aggregate([ 
	{$group : { _id: "$supplier_id", max_price: {$max: "$price" }}}
]);

//Min price of fruit per supplier

db.fruits.aggregate([ 
	{$group : { _id: "$supplier_id", max_price: {$min: "$price" }}}
]);
